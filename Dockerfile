FROM python:3.7

RUN apt-get update \
    && apt-get install -y libpq-dev gcc postgresql-client
COPY requirements.txt requirements.txt
RUN python -m pip install -r requirements.txt
COPY . .
RUN chmod +x entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
